package test;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

public class Main extends Application {
    private boolean playable = true;
    private Boolean turnX= true;
    private Tile[][] board = new Tile[3][3];
    private List<combo> combos = new ArrayList<>();
    private  Pane root = new Pane();

private Parent createContent()
{

    root.setPrefSize(600,600);
    for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
        Tile tile = new Tile();
        tile.setTranslateX(j*200);
        tile.setTranslateY(i*200);
        root.getChildren().add(tile);

        board[j][i] =tile;
        }
    }



    for(int y=0;y<3;y++){
        combos.add(new combo(board[0][y],board[1][y],board[2][y]));
    }
    for(int x=0;x<3;x++){
        combos.add(new combo(board[x][0],board[x][1],board[x][2]));
    }
    combos.add(new combo(board[0][0],board[1][1],board[2][2]));
    combos.add(new combo(board[2][0],board[1][1],board[0][2]));

    return root;
}


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setScene(new Scene(createContent() ));
        stage.show();
    }

    private void CheckState(){
        for(combo combo:combos){
            if(combo.isComplete()){
                playable= false;
                playWin(combo);
                break;
            }
        }
    }

    public void playWin(combo combo1){
        Line line = new Line();
        line.setStartX(combo1.tiles[0].getCenterX());
        line.setStartY(combo1.tiles[0].getCenterY());
        line.setEndX(combo1.tiles[0].getCenterX());
        line.setEndY(combo1.tiles[0].getCenterY());

        //root.getChildren().add(line);


        Timeline timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1) ,new KeyValue(line.endXProperty(),combo1.tiles[2].getCenterX())
        ,new KeyValue(line.endYProperty(),combo1.tiles[2].getCenterY())));
        timeline.play();
    }


    private class combo{
    private Tile[] tiles;
    public combo(Tile... tiles){
        this.tiles=tiles;
    }
    public boolean isComplete(){
        if(tiles[0].getValue().isEmpty())return false;
        return tiles[0].getValue().equals(tiles[1].getValue()) && tiles[0].getValue().equals(tiles[2].getValue());
    }
    }

    private class Tile extends StackPane{
    private Text text= new Text();
        public Tile()
        {
            Rectangle border = new Rectangle(200,200);
            border.setFill(null);
            border.setStroke(Color.BLACK);
            text.setFont(Font.font(72));

            setAlignment(Pos.CENTER);
            getChildren().addAll(border,text);

            setOnMouseClicked(event ->{
                if(!playable) return;

                if(event.getButton() == MouseButton.PRIMARY)
                {

                    if(!turnX) return;
                    drawX();
                    turnX=false;
                    CheckState();

                }
                else if(event.getButton() == MouseButton.SECONDARY){
                    if(turnX) return;
                    drawO();
                    turnX=true;
                    CheckState();
                }
            });
        }

public  double getCenterX(){
            return  getTranslateX()+100;
}

        public  double getCenterY(){
            return  getTranslateX()+100;
        }



        public  String getValue(){
            return text.getText();
        }
        private void drawX(){
            text.setText("X");
        }
        private void drawO(){
            text.setText("O");
        }
    }
}
