module untitled2 {

    requires javafx.media;
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.base;
    requires javafx.fxml;
    requires javafx.web;
    requires javafx.swing;
    requires javafx.swt;

    opens test;
}
